package com.example.datastoragepractice;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.room.Room;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.datastoragepractice.dao.StudentDao;
import com.example.datastoragepractice.database.StudentDatabase;
import com.example.datastoragepractice.databinding.ActivityMainBinding;
import com.example.datastoragepractice.databinding.ShowAllStudentDialogBinding;
import com.example.datastoragepractice.entity.Student;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.io.IOException;
import java.util.List;

public class MainActivity extends BaseActivity<ActivityMainBinding> {
    private String name;
    private String password;
    private String image;
    Student student;
    StudentDao studentDao;
    String DB_NAME = "db_student";
    private Uri selectedImageUri;
    private static final int REQUEST_IMAGE_SELECTION = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StudentDatabase db = Room.databaseBuilder(getApplicationContext(), StudentDatabase.class, DB_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
        studentDao = db.studentDao();
        onShowAllStudentDialog();
        onAddStudent();
        binding.uploadImage.setOnClickListener(view -> {
            imageChooser();
        });
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            selectedImageUri = data.getData();
            binding.uploadImage.setImageURI(selectedImageUri);
        }
    }
    private void imageChooser() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, REQUEST_IMAGE_SELECTION);
    }
    //function add student
    private void onAddStudent() {
        binding.btnAddStudent.setOnClickListener(v -> {
            name = binding.edtName.getText().toString();
            password = binding.edtPassword.getText().toString();
            image = selectedImageUri.toString();
            student = new Student(name, password, image);
            studentDao.insertStudent(student);
            binding.edtName.setText("");
            binding.edtPassword.setText("");
        });
    }
    //function show data all student dialog
    private void onShowAllStudentDialog() {
        binding.btnAddStudentDialog.setOnClickListener( v -> {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
            ShowAllStudentDialogBinding binding1 = ShowAllStudentDialogBinding.inflate(LayoutInflater.from(this));
            List<Student> studentList = studentDao.getAllStudents();
            StringBuilder txtStudent = new StringBuilder();
            for (Student stu : studentList) {
                txtStudent.append(stu.getStu_name()).append("\n\n");
            }
            builder.setTitle("All data in table")
                    .setView(binding1.getRoot())
                    .setMessage(txtStudent);
            AlertDialog dialog = builder.create();
            dialog.show();
        });
    }
    @Override
    public int layout() {
        return R.layout.activity_main;
    }
}