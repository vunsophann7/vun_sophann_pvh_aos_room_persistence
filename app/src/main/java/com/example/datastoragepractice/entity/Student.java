package com.example.datastoragepractice.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
//step 1
@Entity(tableName = "students")
public class Student {
    @PrimaryKey(autoGenerate = true)
    private int stu_id;
    @ColumnInfo(name = "stu_name")
    private String stu_name;
    @ColumnInfo(name = "stu_password")
    private String stu_password;
    @ColumnInfo(name = "image")
    private String image;

    public Student(String stu_name, String stu_password, String image) {
        this.stu_name = stu_name;
        this.stu_password = stu_password;
        this.image = image;
    }

    public Student() {
    }

    public int getStu_id() {
        return stu_id;
    }

    public void setStu_id(int stu_id) {
        this.stu_id = stu_id;
    }

    public String getStu_name() {
        return stu_name;
    }

    public void setStu_name(String stu_name) {
        this.stu_name = stu_name;
    }

    public String getStu_password() {
        return stu_password;
    }

    public void setStu_password(String stu_password) {
        this.stu_password = stu_password;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Student{" +
                "stu_id=" + stu_id +
                ", stu_name='" + stu_name + '\'' +
                ", stu_password='" + stu_password + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
