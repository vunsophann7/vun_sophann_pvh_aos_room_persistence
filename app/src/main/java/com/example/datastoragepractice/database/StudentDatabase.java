package com.example.datastoragepractice.database;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.datastoragepractice.dao.StudentDao;
import com.example.datastoragepractice.entity.Student;

@Database(entities = Student.class, exportSchema = false, version = 3)
public abstract class StudentDatabase extends RoomDatabase {
    public abstract StudentDao studentDao();

}
